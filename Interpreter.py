import pprint
import AST
import SymbolTable
from Memory import *
from Exceptions import *
from visit import *


memory = Memory("pamiec")

class Interpreter(object):
    @on('node')
    def visit(self, node):
        print "@on('node')"

    @when(AST.Program)
    def visit(self, node):
        for decl in node.declarations.list:
            decl.accept2(self)

        #fundefs!

        for instr in node.instructions.list:
            instr.accept2(self)

        print memory

    @when(AST.Declaration)
    def visit(self, node):
        for init in node.inits.list:
            init.accept2(self)

    @when(AST.Init)
    def visit(self, node):
        value = node.expression.accept2(self)[0]
        memory.put(node.id.const, value)

    @when(AST.BinaryExpression)
    def visit(self, node):
        r1 = node.left.accept2(self)[0]
        r2 = node.right.accept2(self)[0]
        return [eval("{0} {1} {2}".format(r1, node.op, r2))]
        # try sth smarter than:
        # if(node.op=='+') return r1+r2
        # elsif(node.op=='-') ...

    # @when(AST.RelOp)
    # def visit(self, node):
    # r1 = node.left.accept(self)
    #     r2 = node.right.accept(self)
    #     # ...

    @when(AST.Assignment)
    def visit(self, node):
        # value = memory.get(node.id.const)
        value = node.expression.accept2(self)[0]
        # print node.expression.accept2(self)[0]
        # print node.id.const, value
        memory.put(node.id.const, value)

    #
    #

    @when(AST.Const)
    def visit(self, node):
        return node.const

    @when(AST.Id)
    def visit(self, node):
        return [memory.get(node.const)]


    # simplistic while loop interpretation
    @when(AST.WhileInstruction)
    def visit(self, node):
        print "whileInstruction"
        r = None
        while node.condition.accept2(self):
            r = node.instruction.accept2(self)
        return r

    @when(AST.PrintInstruction)
    def visit(self, node):
        print node.expression.accept2(self)[0]

    @when(AST.Condition)
    def visit(self, node):
        value = node.expression.accept2(self)[0]
        return value

    @when(AST.CompoundInstruction)
    def visit(self, node):
        for instr in node.instructions.list:
            instr.accept2(self)





