#!/usr/bin/python
import AST
from SymbolTable import SymbolTable, VariableSymbol, FunctionSymbol


ttype = {
    '+': {
        'int': {
            'int': 'int',
            'float': 'float',
        },
        'float': {
            'int': 'float',
            'float': 'float',
        }
    },
    '-': {
        'int': {
            'int': 'int',
            'float': 'float',
        },
        'float': {
            'int': 'float',
            'float': 'float',
        }
    },
    '*': {
        'int': {
            'int': 'int',
            'float': 'float',
        },
        'float': {
            'int': 'float',
            'float': 'float',
        },
        'string': {
            'int': 'string'
        }
    },
    '/': {
        'int': {
            'int': 'float',
            'float': 'float',
        },
        'float': {
            'int': 'float',
            'float': 'float',
        },
    },
    '>': {
        'int': {
            'int': 'int',
            'float': 'int',
        },
        'float': {
            'int': 'int',
            'float': 'int',
        },
        'string': {
            'string': 'int'
        }
    },
    '>=': {
        'int': {
            'int': 'int',
            'float': 'int',
        },
        'float': {
            'int': 'int',
            'float': 'int',
        },
        'string': {
            'string': 'int'
        }
    },
    '<': {
        'int': {
            'int': 'int',
            'float': 'int',
        },
        'float': {
            'int': 'int',
            'float': 'int',
        },
        'string': {
            'string': 'int'
        }
    },
    '<=': {
        'int': {
            'int': 'int',
            'float': 'int',
        },
        'float': {
            'int': 'int',
            'float': 'int',
        },
        'string': {
            'string': 'int'
        }
    },
    '!=': {
        'int': {
            'int': 'int',
            'float': 'int'
        },
        'float': {
            'int': 'int',
            'float': 'int'
        },
        'string': {
            'string': 'int'
        }
    },

}

symbolTable = SymbolTable(None, "pierwszy_scope")


def printError(lineNumber, string):
    print "Line %s:\t %s" % (lineNumber, string)


class NodeVisitor(object):
    def visit(self, node):
        method = 'visit_' + node.__class__.__name__
        visitor = getattr(self, method, self.generic_visit)
        return visitor(node)

    def generic_visit(self, node):
        for child in node.children:
            self.visit(child)


class TypeChecker(NodeVisitor):
    def visit_Declaration(self, node):
        for init in node.inits.list:
            visited = self.visit(init.expression)
            if visited != node.type:
                errorString = "'%s' is type '%s', but expression is '%s'" % (str(init.id), str(node.type), visited)
                printError(init.lineno, errorString)
            if symbolTable.put(str(init.id), VariableSymbol(init.id, node.type)):
                errorString = "redeclaration of '%s'" % (init.id, )
                printError(init.lineno, errorString)

    def visit_PrintInstruction(self, node):
        expressionType = self.visit(node.expression)
        if not expressionType:
            errorString = "undeclared '%s'" % (node.expression, )
            printError(node.lineno, errorString)

    def visit_LabeledInstruction(self, node):
        self.visit(node.instruction)

    def visit_Assignment(self, node):
        if symbolTable.get(node.id.const):
            variableType = symbolTable.get(node.id.const).type
        else:
            errorString = "undeclared '%s'" % (node.id, )
            printError(node.lineno, errorString)
            return

        visited = self.visit(node.expression)
        if not visited:
            errorString = "undeclared '%s'" % (node.expression, ) #!!!bug when: print "In while - x equals: " + x;
            printError(node.lineno, errorString)
        elif variableType != visited:
            errorString = "problem assignment '%s' into '%s'" % (visited, variableType)
            printError(node.lineno, errorString)

    def visit_ChoiceInstructionIfElse(self, node):
        if self.visit(node.condition) != 'int':
            printError(node.lineno, "statement in IF isn't integer")
        self.visit(node.if_instruction)
        self.visit(node.else_instruction)

    def visit_ChoiceInstructionIf(self, node):
        if self.visit(node.condition) != 'int':
            printError(node.lineno, "statement in IF isn't integer")
        self.visit(node.if_instruction)

    def visit_WhileInstruction(self, node):
        if self.visit(node.condition) != 'int':
            printError(node.lineno, "statement in WHILE isn't integer")
        self.visit(node.instruction)

    def visit_RepeatInstruction(self, node):
        if self.visit(node.condition) != 'int':
            printError(node.lineno, "statement in REPEAT isn't integer")
        self.visit(node.instructions)

    def visit_Condition(self, node):
        return self.visit(node.expression)

    def visit_Integer(self, node):
        return 'int'

    def visit_Float(self, node):
        return 'float'

    def visit_String(self, node):
        return 'string'

    def visit_Id(self, node):
        if symbolTable.get(node.const):
            return symbolTable.get(node.const).type
        else:
            return None

    def visit_BinaryExpression(self, node):
        type1 = self.visit(node.left)
        type2 = self.visit(node.right)
        op = node.op

        if not type1:
            errorString = "undeclared '%s'" % (node.left.const, )
            printError(node.left.lineno, errorString)
        if not type2:
            errorString = "undeclared '%s'" % (node.right.const, )
            printError(node.right.lineno, errorString)

        if not (type1 and type2):
            return None
        elif type1 in ttype[op] and type2 in ttype[op][type1]:
            return ttype[op][type1][type2]
        else:
            errorString = "unsupported operation: '%s' %s '%s'" % (type1, op, type2)
            printError(node.lineno, errorString)

    def visit_ParenthesisExpression(self, node):
        return self.visit(node.expression)

    def visit_FunctionCall(self, node):
        lengthOfNodeList = len(node.instruction.list)
        lengthOfArgsList = len(symbolTable.get(node.id).argsList)
        if lengthOfNodeList == lengthOfArgsList:
            for i in xrange(lengthOfNodeList):
                visitedNodeArgument = self.visit(node.instruction.list[i])
                symbolTableArgument = symbolTable.get(node.id).argsList[i]
                if visitedNodeArgument != symbolTableArgument.type:
                    if not (visitedNodeArgument == 'int' and symbolTableArgument.type == 'float'):
                        errorString = "wrong parameter '%s' type '%s' - should be '%s'" % (node.instruction.list[i], visitedNodeArgument, symbolTableArgument.type)
                        printError(node.lineno, errorString)
        else:
            errorString = "wrong number of parameters for function '%s'" % (node.id, )
            printError(node.lineno, errorString)
        return symbolTable.get(node.id).type

    def visit_Fundef(self, node):
        global symbolTable
        if symbolTable.put(str(node.id), FunctionSymbol(node.id, node.type, node.args_list.list)):
            errorString = "redeclaration of '%s'" % (node.id, )
            printError(node.lineno, errorString)
        symbolTable = SymbolTable(symbolTable, node.id.const)
        for arg in node.args_list.list:
            symbolTable.put(arg.id.const, VariableSymbol(arg.id, arg.type))
        self.visit(node.compound_instr)
        symbolTable = symbolTable.getParentScope()
